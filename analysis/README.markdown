# About

This repository contains source code for
re-producing all the results in ../paper.
See the paper for detailed information about the
hows and whys of this analysis.

To build all assets, run

    $ R --vanilla -f do.R

Intermediate steps are saved to avoid unnecessary
re-computation.
The script will not build stuff for which the 
associated `*.RData` file already exists; remove it to
trigger re-computation of that step.

You may also run individual steps, e.g., simulation, by

    $ R --vanilla -f sim.R

Note that this command runs the simulation unconditionally,
overwriting any existing assets (plots, data files).