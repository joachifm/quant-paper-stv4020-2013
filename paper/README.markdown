# About

This repository contains the document source for a paper that I
originally wrote for the course STV4020 at the University of Oslo in
the fall of 2013.
I have subsequently fixed grammatical and formatting errors, but the
substance, warts and all, is more or less intact.
I hope that someone will find this useful, mainly as a technical
example of how to do quantitative political science using GNU R and
Org mode.
As concerns the substance, I do not recommend using it for any other
purpose than as an example of what _not_ to do.

# Abstract

Stegmueller et al (2012) find that religious affiliation is a strong
individual-level predictor of opposition to welfare redistribution by
the government, in particular among persons who belong to one of the
major Christian sects.
I consider the performance of religious affiliation as a binary
classifier of individual-level opposition to welfare redistribution.
The analysis proceeds from a simple logistic binomial regression
model, eschewing the usual battery of socio-demographic controls and
classical statistical inference.

The regression model comprises two complementary measures of religious
affiliation --- belonging to a major Christian sect and frequency of
attendance to religious ceremonies --- which are derived from the same
survey items used by Stegmueller et al (2012).
In addition, I include a measure of perceived income sufficiency, on
the grounds that the effect reported by Stegmueller et al (2012) must
persist regardless of an individual's perceived income sufficiency.
The model is calibrated by applying it to data pooled from the
European Social Survey rounds 5 and 6 (limited to the countries
considered in the original analysis), treating the combined set as a
single population.

My findings are inconsistent with Stegmueller et al's (2012) finding
that religious affiliation is a strong predictor of indvidual-level
opposition to welfare redistribution.
The results indicate that earning a comfortable working salary is a
stronger and more reliable predictor of opposition to welfare
redistribution than is religious affiliation.
When considered as a binary classifier, religious affiliation achieves
a level of accuracy that is only slightly better than tossing a fair
coin.
A likely reason for my failure to obtain consistent results is that
the method is incapable of detecting the effects of the mechanisms
cited by Stegmueller et al (2012) and that a more sophisticated
instrument is required (e.g., one similar to that used in the original
analysis).
