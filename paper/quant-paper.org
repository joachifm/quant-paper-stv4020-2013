#+LaTeX_header: \include{frontpage}
#+LaTeX_header: \usepackage{../../tex/stv4020}

\newpage
\tableofcontents
\listoffigures
\listoftables
\newpage

* Introduction
:PROPERTIES:
:CUSTOM_ID: sec:introduction
:END:

# The research gap and what I do to address it
\citet{stegmueller12} report that religious persons, compared to
secular persons, tend to oppose government efforts to reduce income
differences, taking a wide
array of sociodemographic variables into account.
As an extension of this finding, I use religious affiliation and
participation to define a binary classifier for individuals who oppose
income equalisation and assess its performance when applied to
respondents from the European Social Survey round 5 and 6.

# Problem statement and brief summary of findings
I examine two empirical expectations: (1) that religious individuals
are less supportive of government intervention to redistribute wealth
than are secular individuals; and (2) that persons that frequently
attend religious ceremonies remain opposed to wealth redistribution
even if they themselves are struggling to make ends meet at their
current income.
The difference between religious and secular individuals and the
inverse relationship between participation in religious ceremony and
support for welfare redistribution are the strongest individual-level
effects reported by \citet{stegmueller12}, making them the natural
starting point for analysis of individual-level classification.

# The overall purpose and approach
The overall purpose of this study is to estimate the magnitude of
the effect and predictive force of religiosity on views on wealth
distribution at the individual level.
Unlike \citet{stegmueller12}, I completely disregard contextual
factors and I do not include the usual battery of statistical
controls, choosing instead to emphasise how well a naive model
derived from \citet{stegmueller12} findings performs as a
classifier of individuals who oppose wealth redistribution.
The causal mechanisms that drive the relationship between religiosity
and views on wealth redistribution are taken for granted and not
explored in any detail.

# Plan
I proceed by first defining the concepts and their hypothesised
relationships according to relevant theory.
Having established these theoretical preliminaries, I turn to the data
and variables used for the empirical analysis, discussing whether
available measures adequately capture the concepts.
Then follows a brief description of the estimator of choice and the
formal definition of the model used for classification.
I then present selected technical results, emphasising the magnitude
and precision of the estimated effects, before turning to
assessing the performance of the binary classifier.
Finally, I discuss the substantive implications of the results with
regard to the underlying theory and existing findings.
Detailed results and program code for reproducing the results are
provided in appendices.

** COMMENT Purpose

The introduction establishes the theme and context
and formulates a specific research question, as well
as summarising the general approach and plan for what follows.

Briefly discuss relevant theory as necessary to establish the
research question.
That is, only draw on theory that is necessary for constructing
specific hypotheses and deriving the dependent and independent
variables.

* Theory
:PROPERTIES:
:CUSTOM_ID: sec:theory
:END:

The outcome to be explained is individual opinion about state
intervention to redistribute wealth in an effort to reduce material
inequality.
Traditionally, the issue of wealth redistribution (e.g., progressive
taxes) is seen as an extension of the economic left-right divide.
\citet{stegmueller12} claim that religiosity shapes what people think
about the government's role in welfare provision.
The first mechanism suggested by \citet{stegmueller12} is that
individuals who identify with a religious group will also acquire the
dominant norms in that group.
Christian sects do not oppose provision of welfare as such, but are
opposed specifically to a secular state providing welfare without
regard to moral or religious considerations.
Consequently, persons who identify with a christian denomination, tend
to oppose government intervention \citep[484]{stegmueller12}.
The second proposed mechanism is that participation and integration in
a religious community reduces the demand for welfare from the
government, because participation in a religious community provides
many of the same benefits that one would otherwise get from the state
\citep[484]{stegmueller12}.

According to the first mechanism, persons who belong or identify with
a Christian sect are less supportive of government intervention
because they believe that the government is encroaching on what should
be the domain of the Church.
According to the second mechanism, persons who are well integrated
into a religious community oppose government intervention because
their welfare needs are already met and hence they want the government
to allocate funds for other purposes.
Both mechanisms cause a particular preference or personal disposition,
but not necessarily active opposition or political engagement.

The first mechanism is psychological, whereas the other has a material
basis.
If either or both mechanisms are substantively important, however,
they should persist regardless of the person's income level.
A person who is convinced that government should not intervene in
wealth redistribution should remain so convinced even if he struggles
to make ends meet.
Likewise, a person who struggles to live off his income but is
provided for by his religious community should oppose government
intervention.
Consequently, I will include the person's ability to live comfortably
on their current income as the only rival explanation for opposing
wealth redistribution, on the assumption that working people who are
living comfortably pay more taxes and have less need for support from
the government.
To wit, persons earning a comfortable income have the least to gain
and the most to lose from government efforts to reduce income
differences.

\citet{stegmueller12} include a wide array of sociodemographic
variables that are also thought to influence a person's attitude
to wealth redistribution by the government.
I will not discuss these alternatives, nor include them as statistical
controls.
Excluding these controls is likely to reduce the overall measure of
fit for the model.
The underlying theory does not explicitly state how these
sociodemographic variables are related to any of the primary
concepts under analysis, however.
Lack of explicit theory increases the likelihood that each additional
variable will introduce collinearity that may distort the size and
sign of the estimated effects.
Including these standard controls, then, will make it more difficult
to truly assess the causal impact of each set of variables and
properly interpret the decomposition of variance among them.

I will re-express the theoretical expectations as a binary classifier
and assess to what extent it is capable of correctly identifying
persons who oppose government intervention to reduce income
differences: if religion exerts significant influence on individual
attitudes to redistribution, religious indicators should perform
reasonably when used as a classifier of opposition to
redistribution.
Consequently, I will perform little model diagnostics beyond assessing
the performance of the classifier: either it achieves reasonable
accuracy or it does not.

** COMMENT Purpose

Establish concepts and their relationships according to relevant
theory.

* Data and variables
:PROPERTIES:
:CUSTOM_ID: sec:data
:END:

# Introduce data source
The data are sourced from the fifth and sixth rounds of the European
Social Survey (\citet{ESS5:data}; \citet{ESS6:data}) which comprises
individual-level observations from structured interviews.
Initially, all inhabitants above the age of 15 are eligible to be
included in each national-level sample.
Like \citet{stegmueller12} I only include data from Western European
countries[fn:country-list].
All survey items are provided in the respondent's native language to
ensure a good level of comparability of item responses across the
sample[fn:ess-details], yielding high quality source
data[fn:survey-method].

I pool all data into a single data set of individuals and will not
refer to means or totals, nor will I make any attempt at doing
classical statistical inference from survey sample to population.
The European Social Survey is primarily designed to accommodate
studies of differences between countries, not aggregate analysis
as is done here.
If the goal was statistical inference, the approach I have chosen
would likely produce invalid results.
Rather than attempting to find generaliseable estimates, I treat
opposition to redistribution as a ``signal'' and the model as a device
that detects that signal.

The ESS items cover individual opinions on wealth redistribution,
religious affiliation and participation, and income levels.
The probes in the sixth round of the ESS appear identical to those
from previous rounds and we may be reasonably certain that, at least,
the concepts measured here are the same as those measured by
\citet{stegmueller12}.
Briefly, each concept is operationalised as follows.

The concept to be explained is variation in individual views on wealth
redistribution by the government.
Conveniently, ESS contains an item that asks the respondent to say to
what extent they agree that government should reduce income
differences on a scale that ranges from strong agreement to strong
disagreement.
This is the same probe used by \citet{stegmueller12}.
I am only concerned with whether the respondent opposes
redistribution.
A person who says that they either strongly disagree or disagree with
the statement that the government should reduce income differences is
said to oppose redistribution, otherwise they are said to not oppose
redistribution (this category includes persons who say they neither
agree nor disagree).

The first predictor captures identification or affiliation with a
religious group.
According to the theory and findings of \citet{stegmueller12} there
are three categories of interest: persons affiliated with a Christian
sect, persons affiliated with some other religious group, and secular
individuals (i.e., non-affiliated persons).
Religious affiliation is a nominal variable of three categories,
Secular, Christian, and Other.
The coding was carried out in two steps.
First, respondents who give a negative response to a probe that asks
whether they consider themselves to belong to a specific religion or
denomination are coded as Secular.
Respondents who give a positive answer are coded according to a second
probe asking which denomination they consider themselves as belonging
to.
The Christian category comprises Protestants, Catholics, Orthodox
Christians, and Other Christian; all other denominations are coded as
Other.

The second predictor is intended to capture integration into a
religious community.
Like \citet{stegmueller12}, I have used a probe that asks the
respondent to indicate how often they attend religious ceremonies
(outside of special occasions, such as funerals), on a scale that
ranges from more than once a week to never.
Here I have used the original response as-is, but I reversed the scale
so that greater values correspond to higher frequency of attendance.
This is the weakest operationalisation of the three main variables,
because it does not really tap ``integration into a community''.
The probe says very little about the extent of the respondent's
interaction with the other attendants or the level of social
commitment.
Certainly, it is possible to go to church frequently without ever
getting to know anyone there or to consider it a great social
commitment.
Expecting people integrated into a religious community to frequently
attend religious ceremonies is reasonable, but that frequent
attendance implies integration does not follow.
Nevertheless, going to church frequently increases the likelihood of
getting to know other people who are also frequent church goers, which
may lead to further social commitments and thus integration into a
community organised around religious activity.
More troubling is that the probe fails to capture the extent to which
the respondent receives welfare benefits from belonging to a religious
community.
That is, even if the probe perfectly captures integration, its
relevance to the theory rests entirely on the assumption that
integration into a religious community entails welfare benefits that
at least equal those provided by the state.

The third predictor captures income sufficiency.
This measure is not included by \citet{stegmueller12}, and is
included here mainly because the theory suggests that the effect of
religious affiliation and integration should remain when taking into
account the respondent's ability to make ends meet.
Two probes were used: one asking the respondent to specify their
household's main source of income and one asking the respondent to
indicate whether they are struggling or living comfortably on their
present income.
The resulting variable is coded 1 for respondents who come from
households where wage or self-employment is the main source of income
and who report living comfortably on that income, and 0 for everyone
else.
The expectation is that people who are coded 1 on this variable have
good reason to oppose redistribution because they are the ones from
which income would be redistributed.
If religious affiliation or integration matters, the effect should
remain when taking pure material self-interest into account.

Given the size of the data set (44563 cases), there are relatively
few missing values, most of which are for opposition to redistribution
and earning a comfortable income.
I have not checked that the missing values are missing at random.
Thus, there is a possibility that certain groups of people are
systematically more likely to have missing values.
If these groups are underrepresented already, for whatever reason,
simple listwise exclusion of incomplete cases will skew the sample
further.
Yet, I have opted to exclude all incomplete cases from the data set
before analysis.
First, because imputing is a complicated process, bringing with it its
own risks of introducing bias.
Deleting incomplete cases outright seems a more prudent choice, given
the large sample size.
More important, however, is that I am not trying to make
generalisations from the sample to the population, so that any bias
that comes from listwise exclusion is of little substantive
importance.
The proportion of incomplete cases is so small that it is unfeasible
that excluding incomplete cases should obscure the signal that the
classifier is designed to pick up.
The overall source data set contains 43152 cases after deletion.

[fn:ess-details]
See the data documentation for ESS-5 \citep{ESS5:docs} and ESS-6
\citep{ESS6:docs} for details concerning data collection, interview
protocol, and coding of source data.

[fn:survey-method]
See \citet{harkness03} for general criteria for assessing
cross-country survey data, and \citet{kittilson07} for a general
introduction to the European Social Survey.

[fn:country-list]
The countries are: Portugal, France, Italy, Spain, Finland, Austria,
Ireland, Belgium, Sweden, Norway, Switzerland, Luxembourg (not in
ESS-6), Germany, Great Britain, the Netherlands, and Denmark.

** COMMENT Purpose

- Data
  - What is the population
  - How were the data collected?
    Sampling method and design.
  - Non-response
  - Time of collection

- Operationalisation
  - Discuss central concepts
  - Discuss operationalisation of these concepts:
    are the indicators adequate measures of the concept?
  - Re-coding
  - Handling of missing values
  - Index construction (if applicable)
  - Assess measurement error

* Analysis
:PROPERTIES:
:CUSTOM_ID: sec:analysis
:END:

** Model and method of estimation

Figure \ref{fig:model-formula} is the binomial logistic regression
model[fn:logistic-reference] used to estimate the effect size of each
predictor and that provides the predictions used to classify
opposition to redistribution, where =Oppose.Redist= is the dichotomous
response indicating opposition to redistribution, =Denomination= is a
nominal variable with categories for secular, Christian, and Other
religious denominations, =Attend= is the reported frequency of
attending religious ceremonies, and =Income.Comfort= indicates whether
the respondent main source of income comes from wage labour (or
self-employment) that is sufficient to live comfortably.
The \(\beta_i\) coefficients indicate change in log of odds for the
event that the respondent opposes redistribution associated with a
one-unit increase in the predictor, holding the other predictors
constant.
For =Denomination=, Secular is used as the reference category and
Christian and Other are represented by dummies.
The coefficient for each dummy (not shown here) represents the change
in logits associated with going from Secular to that denomination.
=Attend= is treated as a quasimetric variable and its coefficient
is the change in logits associated with a one-unit increase in the
frequency of attendance, which ranges from never to every day.
Finally, \(Income.Comfort\) is binary and its coefficient is the
change in logits when going from people who do not earn a comfortable
wage to those who do.

#+begin_latex
  \begin{figure}[htb!]
    \[
    \text{Oppose.Redist}
    = \beta_0
    + \beta_1 \text{Denomination}
    + \beta_2 \text{Attend}
    + \beta_3 \text{Income.Comfort}
    \]
    \caption[Model formula]{Predicting opposition to redistribution from
      religious indicators and income}
    \label{fig:model-formula}
  \end{figure}
#+end_latex

Here, the goal of estimation is to correctly predict opposition to
redistribution.
To this end, I randomly assigned 60\% of the initial sample to a
training set, leaving the remainder for testing.
I tuned the model by repeatedly fitting it against the testing set,
but did not perform an exhaustive automated search for the ``best''
classifier.
My primary goal was to identify a small set of indicators that would
enable detection of the effects reported by \citet{stegmueller12}.
I tested many different indicators, which I will not discuss in any
detail.
Most interesting is a measure, available in the most recent ESS
edition, of the respondent's agreement that the government is
currently taking measures to reduce income differences.
This indicator is relevant to the finding reported by
\citet{stegmueller12} that opposition to redistribution tends to
correlate with extensive redistribution.
I was unable to find any such effect, however, and the indicator did
not appear to improve the model's classification performance.

[fn:logistic-reference]
See \citet{hosmer00}, specifically chapters 1--3, for a detailed
introduction to logistic regression analysis.

** Results

#+begin_latex
  \input{../analysis/tbl-model-effects}
#+end_latex

Table \ref{tbl:model-effects} shows the coefficients
and associated 95\% confidence intervals for the model predictors
(when fitted to the training data set)[fn:xtable-ref].
The model fit is partially consistent with \citet{stegmueller12}.
Whereas they find that denomination and attendance are strong
individual-level predictors of opposition to redistribution, the
present model fit confirms that religious affiliation increases
the likelihood of opposing redistribution but the estimated effect for
attendance is negative.
Earning a comfortable wage is a much stronger individual-level
predictor of opposing redistribution than religious affiliation
and participation (as they are measured here), according to the
estimated effect sizes.
Briefly, each coefficient can be summarised as follows.

=Income.Comfortable= is clearly the strongest predictor according to
this model.
The number of successes is not quite small enough (approx. 15\% of
all respondents oppose redistribution; the ratio is the same in the
training data set) to interpret the odds ratios as proportion ratios,
but they are still indicative of estimated effect size.
In addition to having a substantial estimated effect size,
=Income.Comfortable= has a narrow confidence band and appears to be a
reliable predictor.

For =Denomination= we see that Christians are slightly more likely
to oppose redistribution by the government compared to Secular
persons, whereas Other fails to obtain significance.
The estimated effect size for Christians is possibly almost barely
significant, however, with a lower bound at 1.06, but has an upper
bound of 1.24.
That is, the model implies that belonging to a Christian sect might
increase the odds of opposing redistribution, compared to being
Secular, but it might also be nearly irrelevant.
Frequency of attendance is also fairly weak in terms of effect size
and it, too, might be near insignificant.
According to the estimated effect sizes, neither of the religious
predictors fare too well, compared to earning a comfortable wage.

Following the results in \citet{stegmueller12} we expect that
religious individuals, especially Christians, should have greater
``risk'' of opposing redistribution than have secular individuals.
Figure \ref{fig:simulated-quantities} shows simulated
quantities of interest[fn:zelig-ref], comparing Christians who
frequently attend religious ceremonies but not earning a comfortable
income with secular individuals who do earn a comfortable wage.
Clearly, neither group has very high risk of opposing redistribution,
but secular individuals have much higher risk than have Christians,
which is the opposite of the expectation.
Most simulated runs have Christians at between 9\% and 10\%
probability of opposing redistribution and Secular individuals
at twice that (between 22\% and 23\%).

The simulated probability of opposing redistribution for Secular
individuals who make a comfortable wage is greater than the
probability of drawing an individual who opposes redistribution at
random from the sample, whereas the probability for Christians who
frequently attend religious ceremonies and do not earn a comfortable
wage have a smaller probability than random draw (about 15\% of the
individuals in the sample oppose redistribution).
The simulation yields results that are consistent with the coefficient
for =Income.Comfortable=, which under the probability interpretation
of odds ratios indicates a doubling of odds for opposing
redistribution for individuals who earn a comfortable wage, compared
to those who do not, though the effect is smaller than suggested by
the estimate.

Because the main purpose of this model is classification, I have not
performed extensive model diagnostics, either of its technical
assumptions or the model fit.
I have, however, performed k-fold cross validation[fn:boot-ref], which
involves splitting the testing set into k equally sized data sets,
fitting the model for each data set and compute the average prediction
error \citep{davison97}.
I set k such that it divided the testing set into subsamples of 30
cases (testing all permutations of a data set at this size is
prohibitively computationally expensive).
The adjusted average prediction error for this model is \(0.15\).

The purpose of this analysis is not to generalise from the sample to
the population, and doing so would be invalid.
Nevertheless, it is clear that the model as stated does not favour the
hypothesis that religiosity begets opposition to redistribution, at
least when opposition to redistribution is measured as disagreement
with government efforts to reduce income differences.
Rather, it seems that individuals who earn a comfortable income are
far more likely to oppose redistribution, regardless of religious
denomination or the frequency of participation in religious
ceremonies.

#+begin_latex
  \begin{figure}[htb!]
  \includegraphics[scale=0.75]{../analysis/sim.pdf}
  \caption[Simulated quantities of interest]{Simulated quantities
    of interest showing the difference between Christian and
    secular respondents.
    To the left are predicted values and expected values for
    Christians who frequently attend religious services but who
    do not earn a comfortable wage.
    To the right are simulated quantities for secular persons
    who earn a comfortable wage.}
  \label{fig:simulated-quantities}
  \end{figure}
#+end_latex

[fn:xtable-ref]
All tables were produced by the =xtable= package \citep{dahl13}.

[fn:zelig-ref]
The simulated quantities of interest and plot was produced by the
=Zelig= package \citep{owen13}.

[fn:boot-ref]
Cross validation was done by the =boot= package \citep{canty13}.

** Performance

The model is intended to classify persons who oppose redistribution,
based on their religious affiliation and whether they earn a
comfortable income.
After tuning the model against the training data set, I fitted the
model to the testing set, producing a set of model predictions for the
new data.
A binary classifier sorts observations into two bins, according to
whether their predicted probability of having the property in question
exceeds a given threshold.
The question now is how well the model will perform over a range of
thresholds.

Most important when assessing a classifier is its ability to
discriminate true hits from false hits, how good it is at separating
the signal from the noise.
Generally, a good classifier will be both sensitive and specific.
A sensitive classifier is good at picking up observations that have
the outcome; a specific classifier is good at avoiding picking up
observations that do not have the outcome.
Both sensitivity and specificity depend on the classification
threshold.
Specificity can easily be increased by using a higher threshold, but a
higher threshold usually also means lower sensitivity.
Likewise, sensitivity is easily improved by lowering the threshold, at
the cost of lower specificity.
At the extremes, the classifier will either identify all observations
as positive (maximum sensitivity) or none (maximum specificity).

Figure \ref{fig:ROC-plot} is the Receiver Operator Curve[fn:ROC-ref]
for the classifier.
The ROC curve illustrates the trade-off between sensitivity and
specificity over a range of thresholds \citep[160]{hosmer00}.
The rate of true positives shows how often the classifier correctly
identifies a respondent who opposes redistribution while the rate of
false positives shows how often the classifier classifies a respondent
as opposing redistribution when he in fact does not.
Good accuracy is achieved when the true positive rate is high and the
false positive rate is low, giving a vertical curve from the origin
(and maximising the area below the curve).
A straight diagonal from the origin would indicate that the classifier
counts every other observation as a positive, giving an equal number
of true positives and false positives.
Such a classifier is about as useful as tossing a fair coin
\citep[160]{hosmer00}.
Here, it seems that we can achieve \(0.4\) true positives against
\(0.2\) false positives or nearly \(0.6\) true positives for \(0.4\)
false positives, which means that the model is at least slightly
better than a coin, but the curve rapidly degenerates to a diagonal as
the sensitivity increases.
The area under the ROC curve is \(0.606\), falling short of \(0.7\)
which is regarded as the lowest acceptable accuracy
\citep[160]{hosmer00}.

#+begin_latex
  \begin{figure}[htb!]
  \includegraphics[scale=0.75]{../analysis/ROC-plot.pdf}
  \label{fig:ROC-plot}
  \caption[Receiver Operator Characteristic curve]{
    Receiver Operator Characteristic plot.
    The true positive rate and the false positive rate are shown in the
    Y and X axes, as they vary with the classification threshold.
    A perfect classifier has a true positive rate of 1 and
    false positive rate of 0 (the upper left corner).
    A straight diagonal curve indicates an equal rate of
    true and false positives, which means that the classifier is about
    as good as a fair coin toss.
    This model is slightly better than tossing a coin for classifying
    respondents who say they oppose government efforts to equalise
    income differences.
  }
  \end{figure}

#+end_latex

[fn:ROC-ref]
The ROC curve was produced by the =ROC= package \citep{sing05}.

** COMMENT Purpose

- The model
- Are the model assumptions satisfied?
- Are the results robust over samples
- Predictive performance?

* Conclusion
:PROPERTIES:
:CUSTOM_ID: sec:conclusion
:END:

\citet{stegmueller12} report that religious individuals
tend to oppose government efforts to reduce income differences.
I have considered this as a classification problem, testing the
performance of a model that predicts opposition to redistribution
from religious affiliation, frequency of attending religious
ceremonies, and earning a comfortable income.
Unlike previous research, the present study excludes common
statistical controls and contextual variables, focusing narrowly on
predictive performance and estimation of effect sizes at the
individual level.

I found that indicators of religiosity are poor predictors of
individual attitudes to wealth redistribution, operationalised as
disagreement with government efforts to reduce income differences.
The estimated effects are inconsistent with the hypothesis that
religious persons have a greater tendency to oppose government
provision of welfare than have secular persons, rather indicating that
earning a comfortable income is a stronger and more reliable predictor
of opposition to redistribution.
The model performs poorly as a classifier, however, achieving
unacceptable accuracy.

The results I have found cast some doubt on the general idea that
religion is a strong predictor of individual-level variation in
attitudes to wealth redistribution, and possibly government
intervention in general.
Nevertheless, there may be substantial conditional effects, e.g.,
religious polarisation (as reported by \citet{stegmueller12}), that
are undetectable when all respondents are considered as a single
population (of European citizens), as I have done.
Moreover, the model I fitted is exceedingly naive, compared to the
rather complicated model used by \citet{stegmueller12}.
It could be that the effect of religious affiliation requires a more
sophisticated measuring device.
The premise that the effect of religiosity on opposition to
redistribution should be detectable using a naive logistic regression
model could be unreasonable in itself, which would obviate my entire
argument.
Investigating the effect of contextual variables for classification
performance, possibly using more sophisticated model specifications
and method of estimation, is a natural extension of the present model
and a possible avenue for future research in this vein.

\newpage
\bibliography{references}
\newpage
\appendix

** COMMENT Purpose

- What the analysis tells us about the research question
- The results in light of the data and the assumptions:
  what can we reasonably infer about the real world from these
  results?
  That is, are the conclusions valid?
- Concluding remarks

* Syntax
:PROPERTIES:
:CUSTOM_ID: sec:syntax
:eval: never
:exports: code
:tangle: no
:END:

Here follows the complete source code listing for data preparation and
analysis (including plots).
To reproduce all the results presented in this paper, download the
data archives from the European Social Survey web site and load the
program code into GNU R \citep{R}.
Minimal comments are provided, for brevity, but the code should be
self-explanatory even to users not familiar with the syntax and
semantics of GNU R (all the heavy lifting is done by third-party
packages).

** Dependencies

#+begin_src R
  install.packages(c("xtable", "Zelig", "ROCR", "boot"))
#+end_src

** Data preparation

#+include: "../analysis/load.R" src R

** Model fit

#+include: "../analysis/fit.R" src R

** Simulate quantities of interest

#+include: "../analysis/sim.R" src R

** Classifier performance

#+include: "../analysis/performance.R" src R

** Cross validation

#+include: "../analysis/validate.R" src R

* Detailed results
:PROPERTIES:
:CUSTOM_ID: sec:detailed-results
:eval: yes
:exports: results
:results: output
:tangle: no
:session: *R*
:cache: yes
:END:

#+begin_src R :exports none
  load("../analysis/fit.RData")
#+end_src

#+RESULTS[6ed78df05981642d25f5b4dbe49f6cc08ec6945d]:

** Numerical summaries

#+begin_src R
  summary(mdl.dat)
#+end_src

#+RESULTS[9e3e8b1f28a82f5dfbab3780e3b1c604cf00323a]:
#+begin_example
 Oppose.Redist      Denomination       Attend
 Mode :logical   None     :20158   Min.   :1.00
 FALSE:37250     Other    : 1573   1st Qu.:1.00
 TRUE :6688      Christian:22732   Median :2.00
 NA's :625       NA's     :  100   Mean   :2.27
                                   3rd Qu.:3.00
                                   Max.   :7.00
                                   NA's   :121
 Income.Comfortable
 Mode :logical
 FALSE:31507
 TRUE :12408
 NA's :648
#+end_example

** Cross tabulation
*** Opposing redistribution versus denomination

#+begin_src R
  xtabs(~ Oppose.Redist + Denomination, mdl.dat)
#+end_src

#+RESULTS[496b6f86e1e8fdf2fc8e07a48ca7ea153ffbf597]:
:              Denomination
: Oppose.Redist  None Other Christian
:         FALSE 16810  1285     19081
:         TRUE   3114   220      3338

*** Opposing redistribution versus earning a comfortable wage

#+begin_src R
  xtabs(~ Oppose.Redist + Income.Comfortable, mdl.dat)
#+end_src

#+RESULTS[1023717523516ccdfb579bf2e7b48dab083f6fe9]:
:              Income.Comfortable
: Oppose.Redist FALSE  TRUE
:         FALSE 27392  9344
:         TRUE   3646  2948

*** Opposing redistribution versus frequency of attending religious ceremonies

#+begin_src R
  xtabs(~ Oppose.Redist + Attend, mdl.dat)
#+end_src

#+RESULTS[0d6f4647e3207d78a394bd5a726e7d27a1c17b76]:
:              Attend
: Oppose.Redist     1     2     3     4     5     6     7
:         FALSE 15275  8269  6501  3107  2952   841   204
:         TRUE   2861  1522  1310   484   364   121    20

** Regression output

#+begin_src R
  summary(mdl.fit)
#+end_src

#+RESULTS[f6711d8d6dd53d5512bb6a623e1ea54262315894]:
#+begin_example

Call:
glm(formula = mdl.fml, family = binomial(link = logit), data = mdl.dat[train.idx,
    ])

Deviance Residuals:
   Min      1Q  Median      3Q     Max
-0.793  -0.532  -0.498  -0.477   2.219

Coefficients:
                       Estimate Std. Error z value Pr(>|z|)
(Intercept)             -1.9645     0.0377  -52.06  < 2e-16
Income.ComfortableTRUE   0.8478     0.0359   23.64  < 2e-16
DenominationOther        0.1809     0.1024    1.77  0.07719
DenominationChristian    0.1379     0.0410    3.36  0.00077
Attend                  -0.0583     0.0148   -3.94  8.3e-05

(Dispersion parameter for binomial family taken to be 1)

    Null deviance: 21994  on 25890  degrees of freedom
Residual deviance: 21429  on 25886  degrees of freedom
AIC: 21439

Number of Fisher Scoring iterations: 4
#+end_example

* Questions
:PROPERTIES:
:CUSTOM_ID: sec:questions
:END:

These are the European Social Survey items used for this analysis.
Refer to the data documentation for a more detailed overview.

** =gincdif=: government should reduce differences in income levels

B26-27: ``please say to what extent you agree that the government
should take measures to reduce differences in income levels''
The values are: 1 (agree strongly), 2 (agree), 3 (neither agree nor
disagree), 4 (disagree), 5 (disagree strongly), and 7, 8, and 9 are
missing values.

** =hincsrca=: main source of household income

F40: ``Please consider the income of all household members and any
income which may be received by the household as a whole. What is the
main source of income in your household?''
The values are: 1 (wages and salaries), 2 (income from self-employment
(excl. farming)), 3 (income from farming), 4 (pensions), 5
(unemployment), 6 (any other social benefit or grant), 7 (income from
investments, savings), 8 (income from other sources), and 77, 88, and
99 are missing values.

** =hincfel=: feeling about household's income nowadays

F42: ``Which of the descriptions on this card comes closest to how you
feel about you household's income nowadays?''.
The values are: 1 (living comfortably), 2 (coping on present income),
3 (difficult on present income), 4 (very difficult on present income),
and 7, 8, and 9 are missing values.

** =rlgblg=: belonging to particular religion or denomination

C9: ``Do you consider yourself as belonging to any particular religion
or denomination?''.
The values are: 1 (yes) and 2 (no), and 7, 8, and 9 are missing.

** =rlgdnm=: religion or denomination belonging to at present

If respondents gave a positive answer to C9, they are asked
to clarify which religion or denomination they belong to (C10).
The values are: 1 (Roman catholic), 2 (Protestant), 3 (Eastern
Orthodox), 4 (Other Christian), 5 (Jewish), 6 (Islamic),
7 (Eastern religions), 8 (Other non-Christian religions),
and 66, 77, and 99 are missing values.

** =rlgatnd=: how often attend religious services apart from special occasions

C14: ``Apart from special occasions such as weddings and
funerals, about how often do you attend religious services
nowadays?''.
The values are: 1 (every day), 2 (more than once a week), 3 (once a
week), 4 (at least once a month), 5 (only on special holy days), 6
(less often), 7 (never), and 77, 88, and 99 are missing.

* COMMENT Export options

#+OPTIONS: ':nil *:t -:t ::t <:t H:3 \n:nil ^:t arch:headline
#+OPTIONS: author:t c:nil creator:comment d:(not "LOGBOOK") date:nil
#+OPTIONS: e:t email:nil f:t inline:t num:t p:nil pri:nil prop:nil
#+OPTIONS: stat:t tags:nil tasks:t tex:t timestamp:t toc:nil todo:nil |:t
#+CREATOR: Emacs 24.3.1 (Org mode 8.2.3b)
#+DESCRIPTION:
#+EXCLUDE_TAGS: noexport
#+KEYWORDS:
#+LANGUAGE: en
#+SELECT_TAGS: export
